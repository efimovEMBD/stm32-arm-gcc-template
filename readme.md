# STM32 development environment with Visual Studio Code and CMake

## Template Description 
Provided template project is designed to be runned under VSCode. Just open the folder with the project source and start `config/build/run/debug` actions. 

### Template dependencies

1. [CMake](https://cmake.org/). To **install on Ubuntu** machine run:
```shell
sudo apt-get install cmake
``` 
2. [arm-none-eabi-gcc toolchain](https://developer.arm.com/downloads/-/arm-gnu-toolchain-downloads). To **install on Ubuntu** machine just use the following: 
```shell
sudo apt install gcc-arm-embedded
```

3. [Ninja]() or [Make]()
```shell
sudo apt install ninja-build
```
or 
```shell
sudo sudo apt install make
```

> **`Note 1:`** This project depends on [compile-time-init-build](https://github.com/intel/compile-time-init-build) or to be shor - `cib` by Intel. To be able to compile this library it's required to set CXX-compiler support of `-std=gnu++20` or `-std=gnu++17` flag. This is possible only if using [arm-none-eabi-gcc toolchain](). IAR-toolchain can't compile this library as it was said before. 

> **`Note 2:`** IAR-toolchain is not support by this template project (coz IAR-compiler **is not** support pure `-std=gnu++17`, but only `-std=gnu++14` which **is not** compatible with `cib`)



This template-project was designed to be run under Ubuntu, but it's not neccesary to use only this OS. Template can be used also on Windows-based machines (tested) and OSX-based (not tested). 

## Template Usage using CLI

This template uses cmake presets to configure and build target. 

**How to configure** 

Open root of the project folder in `CLI` and run: 
```shell
cmake --preset <debug|release> 
```

**How to build** 
```shell
cmake --build --preset <debug|release>
```

![template-usage](./refs/figures/template-usage.png)